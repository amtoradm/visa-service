package com.retelzy.visa.repository;

import java.util.Date;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.retelzy.visa.entity.Visa;

@Repository
public interface VisaRepository extends JpaRepository<Visa, Long> {

	@Query(value = "select * from visa where user_id=?1", nativeQuery = true)
	Visa getVisa(Long userId);

	@Query(value = "select * from visa where user_id=?1", nativeQuery = true)
	Integer checkRecordInVisa(Long userId);

	@Modifying
    @Transactional
	@Query(value = "update visa set isuscitizen=?1, is_nonuscitizen=?2, is_emplawful_permanent_resident=?3, is_authorized_to_work=?4, perm_alien_reg_no=?5, alien_auth_to_work_reg_no=?6, alien_auth_to_worki94admi_no=?7, alien_auth_to_work_foreign_passport_num=?8, alien_auth_to_work_foreign_passport_counry=?9, auth_work_exp_date=?10, doc_title=?11, doc_number=?12, doc_exp_date=?13, visa_category=?14, visa_earliest_renewal_date=?15, visa_latest_renewal_date=?16, visa_exp_date=?17 where user_id=?18", nativeQuery = true)
	Integer updateVisa(String isUSCitizen, String isNonUSCitizen, String isEmplawfulPermanentResident,
			String isAuthorizedToWork, String permAlienRegNo, String alienAuthToWorkRegNo,
			String alienAuthToWorkI94AdmiNo, String alienAuthToWorkForeignPassportNum,
			String alienAuthToWorkForeignPassportCountry, Date authWorkExpDate, String docTitle, String docNumber,
			Date docExpDate, String visaCategory, Date visaEarliestRenewalDate,
			Date visaLatestRenewalDateLong, Date visaExpDate, Long userId);

}
