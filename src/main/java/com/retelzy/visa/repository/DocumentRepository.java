package com.retelzy.visa.repository;

import java.util.Date;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.retelzy.visa.entity.Document;

@Repository
public interface DocumentRepository extends JpaRepository<Document, Long> {

	@Query(value = "select * from document where user_id=?1", nativeQuery = true)
	Document getDocument(Long userId);


	@Query(value = "select * from document where user_id=?1", nativeQuery = true)
	Integer checkRecordInDocument(Long userId);

	@Modifying
    @Transactional
	@Query(value = "update document set document_title=?1, issuing_authority=?2, document_number=?3, exp_date=?4, additional_info=?5 where user_id=?6", nativeQuery = true)
	Integer updateDocument(String documentTitle, String issuingAuthority, String documentNumber, Date expDate,
			String additionalInfo, Long userId);
	
	@Modifying
    @Transactional
	@Query(value = "update document set is_documents_upload=1 where user_id=?1", nativeQuery = true)
	Integer documentsUpload(String userId);

	@Query(value = "select * from document where user_id=?1 and is_documents_upload=1", nativeQuery = true)
	Integer isDocumentsUpload(String userId);
}
