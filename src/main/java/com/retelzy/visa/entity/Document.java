package com.retelzy.visa.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Document {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long docId;
    @Column(unique=true)
    private Long userId;
    private String documentTitle;
    private String issuingAuthority;
    private String documentNumber;
    private Date expDate;
    private String additionalInfo;
    private Long isDocumentsUpload;
}
