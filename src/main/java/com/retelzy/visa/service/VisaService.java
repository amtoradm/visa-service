package com.retelzy.visa.service;
import com.retelzy.visa.entity.Document;
import com.retelzy.visa.entity.Visa;
import com.retelzy.visa.model.DocumentModel;
import com.retelzy.visa.model.VisaModel;

public interface VisaService {
	
	Visa saveVisa(VisaModel VisaModel);

	Document saveDocument(DocumentModel documentModel);

	Visa getVisaInfo(Long userId);

	Document getDocumentInfo(Long userId);

	Integer checkRecordInDocument(Long userId);

	Integer updateVisa(VisaModel visaModel);

	Integer updateDocument(DocumentModel documentModel);

	Integer checkRecordInVisa(Long userId);
}
