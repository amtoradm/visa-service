package com.retelzy.visa.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.retelzy.visa.entity.Document;
import com.retelzy.visa.entity.Visa;
import com.retelzy.visa.model.DocumentModel;
import com.retelzy.visa.model.VisaModel;
import com.retelzy.visa.repository.DocumentRepository;
import com.retelzy.visa.repository.VisaRepository;

@Service
public class VisaServiceImpl implements VisaService {

	@Autowired
	private VisaRepository visaRepository;
	
	@Autowired
	private DocumentRepository documentRepository;


	@Override
	public Visa saveVisa(VisaModel visaModel) {
		Visa visa = new Visa();
		
		visa.setUserId(visaModel.getUserId());
		visa.setIsUSCitizen(visaModel.getIsUSCitizen());
	    visa.setIsNonUSCitizen(visaModel.getIsNonUSCitizen());
	    visa.setIsEmplawfulPermanentResident(visaModel.getIsEmplawfulPermanentResident());
	    visa.setIsAuthorizedToWork(visaModel.getIsAuthorizedToWork());
	    visa.setPermAlienRegNo(visaModel.getPermAlienRegNo());
	    visa.setAlienAuthToWorkRegNo(visaModel.getAlienAuthToWorkRegNo());
	    visa.setAlienAuthToWorkI94AdmiNo(visaModel.getAlienAuthToWorkI94AdmiNo());
	    visa.setAlienAuthToWorkForeignPassportNum(visaModel.getAlienAuthToWorkForeignPassportNum());
	    visa.setAlienAuthToWorkForeignPassportCountry(visaModel.getAlienAuthToWorkForeignPassportCountry());
	    visa.setAuthWorkExpDate(visaModel.getAuthWorkExpDate());
	    visa.setDocTitle(visaModel.getDocTitle());
	    visa.setDocNumber(visaModel.getDocNumber());
	    visa.setDocExpDate(visaModel.getDocExpDate());
	    visa.setVisaCategory(visaModel.getVisaCategory());
	    visa.setVisaEarliestRenewalDate(visaModel.getVisaEarliestRenewalDate());
	    visa.setVisaLatestRenewalDate(visaModel.getVisaLatestRenewalDate());
	    visa.setVisaExpDate(visaModel.getVisaExpDate());
		 
		visaRepository.save(visa);
		return visa;
	}

	@Override
	public Document saveDocument(DocumentModel documentModel) {
		
		Document document = new Document();
		
		document.setUserId(documentModel.getUserId());
		document.setDocumentTitle(documentModel.getDocumentTitle());
	    document.setIssuingAuthority(documentModel.getIssuingAuthority());
	    document.setDocumentNumber(documentModel.getDocumentNumber());
	    document.setExpDate(documentModel.getExpDate());
	    document.setAdditionalInfo(documentModel.getAdditionalInfo());
	    documentRepository.save(document);
		return document;
	}

	@Override
	public Visa getVisaInfo(Long userId) {
		return visaRepository.getVisa(userId);
	}

	@Override
	public Document getDocumentInfo(Long userId) {
		return documentRepository.getDocument(userId);
	}

	@Override
	public Integer checkRecordInVisa(Long userId) {
		Integer record = visaRepository.checkRecordInVisa(userId);
		return record;
	}

	@Override
	public Integer checkRecordInDocument(Long userId) {
		Integer record = documentRepository.checkRecordInDocument(userId);
		return record;
	}

	@Override
	public Integer updateVisa(VisaModel visaModel) {
		Visa v = new Visa();
		
		v.setUserId(visaModel.getUserId());
		v.setIsUSCitizen(visaModel.getIsUSCitizen());
	    v.setIsNonUSCitizen(visaModel.getIsNonUSCitizen());
	    v.setIsEmplawfulPermanentResident(visaModel.getIsEmplawfulPermanentResident());
	    v.setIsAuthorizedToWork(visaModel.getIsAuthorizedToWork());
	    v.setPermAlienRegNo(visaModel.getPermAlienRegNo());
	    v.setAlienAuthToWorkRegNo(visaModel.getAlienAuthToWorkRegNo());
	    v.setAlienAuthToWorkI94AdmiNo(visaModel.getAlienAuthToWorkI94AdmiNo());
	    v.setAlienAuthToWorkForeignPassportNum(visaModel.getAlienAuthToWorkForeignPassportNum());
	    v.setAlienAuthToWorkForeignPassportCountry(visaModel.getAlienAuthToWorkForeignPassportCountry());
	    v.setAuthWorkExpDate(visaModel.getAuthWorkExpDate());
	    v.setDocTitle(visaModel.getDocTitle());
	    v.setDocNumber(visaModel.getDocNumber());
	    v.setDocExpDate(visaModel.getDocExpDate());
	    v.setVisaCategory(visaModel.getVisaCategory());
	    v.setVisaEarliestRenewalDate(visaModel.getVisaEarliestRenewalDate());
	    v.setVisaLatestRenewalDate(visaModel.getVisaLatestRenewalDate());
	    v.setVisaExpDate(visaModel.getVisaExpDate());
		 
		return visaRepository.updateVisa(v.getIsUSCitizen(),
				v.getIsNonUSCitizen(),
				v.getIsEmplawfulPermanentResident(),
				v.getIsAuthorizedToWork(),
				v.getPermAlienRegNo(),
				v.getAlienAuthToWorkRegNo(),
				v.getAlienAuthToWorkI94AdmiNo(),
				v.getAlienAuthToWorkForeignPassportNum(),
				v.getAlienAuthToWorkForeignPassportCountry(),
				v.getAuthWorkExpDate(),
				v.getDocTitle(),
				v.getDocNumber(),
				v.getDocExpDate(),
				v.getVisaCategory(),
				v.getVisaEarliestRenewalDate(),
				v.getVisaLatestRenewalDate(),
				v.getVisaExpDate(),
				v.getUserId()
				);
	}

	@Override
	public Integer updateDocument(DocumentModel documentModel) {
Document d = new Document();
		
		d.setUserId(documentModel.getUserId());
		d.setDocumentTitle(documentModel.getDocumentTitle());
	    d.setIssuingAuthority(documentModel.getIssuingAuthority());
	    d.setDocumentNumber(documentModel.getDocumentNumber());
	    d.setExpDate(documentModel.getExpDate());
	    d.setAdditionalInfo(documentModel.getAdditionalInfo());
		return documentRepository.updateDocument(d.getDocumentTitle(),
				d.getIssuingAuthority(),
				d.getDocumentNumber(),
				d.getExpDate(),
				d.getAdditionalInfo(),
				d.getUserId());
	}
}
