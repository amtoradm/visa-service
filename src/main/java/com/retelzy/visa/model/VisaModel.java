package com.retelzy.visa.model;

import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class VisaModel {
    private Long userId;
    private String isUSCitizen;
    private String isNonUSCitizen;
    private String isEmplawfulPermanentResident;
    private String isAuthorizedToWork;
    private String permAlienRegNo;
    private String alienAuthToWorkRegNo;
    private String alienAuthToWorkI94AdmiNo;
    private String alienAuthToWorkForeignPassportNum;
    private String alienAuthToWorkForeignPassportCountry;
    private Date authWorkExpDate;
    private String docTitle;
    private String docNumber;
    private Date docExpDate;
    private String visaCategory;
    private Date visaExpDate;
    private Date visaEarliestRenewalDate;
    private Date visaLatestRenewalDate;
}
