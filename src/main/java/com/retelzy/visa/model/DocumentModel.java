package com.retelzy.visa.model;

import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class DocumentModel {
    private Long userId;
    private String documentTitle;
    private String issuingAuthority;
    private String documentNumber;
    private Date expDate;
    private String additionalInfo;
}
