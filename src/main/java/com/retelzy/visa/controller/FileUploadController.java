package com.retelzy.visa.controller;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.method.annotation.MvcUriComponentsBuilder;

import com.retelzy.visa.model.FileInfo;
import com.retelzy.visa.response.Response;
import com.retelzy.visa.service.FilesStoreService;

import lombok.extern.slf4j.Slf4j;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@Slf4j
public class FileUploadController {

  @Autowired
  FilesStoreService storageService;

  @PostMapping("/upload")
  public ResponseEntity<Response> uploadFile(@RequestParam("file") MultipartFile file, @RequestParam("userId") String userId,  @RequestParam("email") String email ) {
    String message = "";
    try {
    	storageService.init(userId,email);
      storageService.save(file);

      message = "File successfully uploaded: " + file.getOriginalFilename();
      return ResponseEntity.status(HttpStatus.OK).body(new Response(message));
    } catch (Exception e) {
      message = "Could not upload the file: " + file.getOriginalFilename() + "!" ;
      return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(new Response(message));
    }
  }

  @GetMapping("/files")
  public ResponseEntity<List<FileInfo>> getListFiles() {
    List<FileInfo> fileInfos = storageService.loadAll().map(path -> {
      String filename = path.getFileName().toString();
      String url = MvcUriComponentsBuilder
          .fromMethodName(FileUploadController.class, "getFile", path.getFileName().toString()).build().toString();

      return new FileInfo(filename, url);
    }).collect(Collectors.toList());

    return ResponseEntity.status(HttpStatus.OK).body(fileInfos);
  }

  @GetMapping("/files/{filename:.+}")
  public ResponseEntity<Resource> getFile(@PathVariable String filename) {
    Resource file = storageService.load(filename);
    return ResponseEntity.ok()
        .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + file.getFilename() + "\"").body(file);
  }
}
