package com.retelzy.visa.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.retelzy.visa.entity.Document;
import com.retelzy.visa.entity.Visa;
import com.retelzy.visa.model.DocumentModel;
import com.retelzy.visa.model.VisaModel;
import com.retelzy.visa.repository.VisaRepository;
import com.retelzy.visa.response.MessageResponse;
import com.retelzy.visa.service.VisaService;

import javax.servlet.http.HttpServletRequest;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@Slf4j
public class VisaController {

	@Autowired
	private VisaService visaService;

	@Autowired
	VisaRepository visaRepository;

	@PostMapping("/visa")
	public ResponseEntity<?> saveVisa(@RequestBody VisaModel visaModel, final HttpServletRequest request) {
		Integer record = visaService.checkRecordInVisa(visaModel.getUserId());
		if(record==null) {
			visaService.saveVisa(visaModel);
		}else {
			visaService.updateVisa(visaModel);
		}
		return ResponseEntity.ok(new MessageResponse("Visa Data saved successfully!"));
	}
	
	@PostMapping("/document")
	public ResponseEntity<?> saveDocument(@RequestBody DocumentModel documentModel, final HttpServletRequest request) {
		Integer record = visaService.checkRecordInDocument(documentModel.getUserId());
		if(record==null) {
			visaService.saveDocument(documentModel);
		}else {
			visaService.updateDocument(documentModel);
		}
		return ResponseEntity.ok(new MessageResponse("Document Data saved successfully!"));
	}
	
	@GetMapping(value = "/getVisa")
	public ResponseEntity<?> getVisa(@RequestParam("email") Long userId) {
		Visa visa = visaService.getVisaInfo(userId);
		if (visa != null) {
			return new ResponseEntity<>(visa, HttpStatus.OK);
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}

	@GetMapping(value = "/getDocument")
	public ResponseEntity<?> getDocument(@RequestParam("email") Long userId) {
		Document document = visaService.getDocumentInfo(userId);
		if (document != null) {
			return new ResponseEntity<>(document, HttpStatus.OK);
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}
	
}
