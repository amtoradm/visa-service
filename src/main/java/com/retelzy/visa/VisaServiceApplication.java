package com.retelzy.visa;

import javax.annotation.Resource;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.web.client.RestTemplate;

import com.retelzy.visa.service.FilesStoreService;

@SpringBootApplication
//@EnableEurekaClient
public class VisaServiceApplication {

	 @Resource
	  FilesStoreService storageService;
	 
	public static void main(String[] args) {
		SpringApplication.run(VisaServiceApplication.class, args);
	}
	
	/*
	 * @Override public void run(String... arg) throws Exception {
	 * storageService.deleteAll(); storageService.init(); }
	 */
	
	/*
	 * @Bean
	 * 
	 * @LoadBalanced public RestTemplate restTemplate() { return new RestTemplate();
	 * }
	 */

}
